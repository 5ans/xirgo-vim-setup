" Vim syntax file
" Language:             Testboy (Xirgo) Case File
" Maintainer:           Ryan Herbison <rherbison@xirgotech.com>
" Latest Revision:      2018-04-16
" License:              Vim (see :h license)
" Repository:           TBD

if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

syn keyword tbcTodo contained @todo @note @review
syn match tbcComment '--.*$' contains=tbcTodo

syn match tbcPlaceholder '__[_0-9A-Z]*__'
syn match tbcVar '${[_0-9A-Z]*}'
syn region tbcStr start=+\(L\|u\|u8\|U\|R\|LR\|u8R\|uR\|UR\)\="+ end='"' contains=tbcPlaceholder contains=tbcVar

syn match tbcSep ':' contained
syn match tbcNum '[.0-9]' contains=tbcDec

syn keyword cmdKeyword     RUNCMD  nextgroup=tbcSep skipwhite
syn keyword portcmdKeyword PORTCMD nextgroup=tbcSep skipwhite
syn keyword untilKeyword   UNTIL   nextgroup=tbcSep skipwhite
syn keyword runokKeyword   RUNOK   nextgroup=tbcSep skipwhite
syn keyword waitResKeyword WAITRES nextgroup=tbcSep skipwhite
syn keyword promptKeyword  PROMPT  nextgroup=tbcSep skipwhite
syn keyword delayKeyword   DELAY   nextgroup=tbcSep skipwhite
syn keyword filterKeyword  FILTER  nextgroup=tbcSep skipwhite
syn keyword cfilterKeyword CFILTER nextgroup=tbcSep skipwhite
syn keyword commonKeyword  EOT EOF ON OFF

let b:current_syntax = "tbc"

hi def link tbcTodo        Todo
hi def link tbcComment     Comment
hi def link tbcStr         String
hi def link tbcNum         PreProc
hi def link tbcDec         PreProc
hi def link tbcPlaceholder Keyword
hi def link tbcVar         Keyword

hi def link cmdKeyword     Type
hi def link portcmdKeyword Type
hi def link untilKeyword   Type
hi def link runokKeyword   Type
hi def link waitResKeyword Type
hi def link promptKeyword  Type
hi def link delayKeyword   Type
hi def link filterKeyword  Type
hi def link cfilterKeyword Type
hi def link commonKeyword  Identifier

let &cpo = s:cpo_save
unlet s:cpo_save
