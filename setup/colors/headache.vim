" Maintainer:  Jacob Foell (jfoell@gmail.com)
" Last Change: 08-Mar-2018

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "headache"

"" General colors
hi CursorLine                   guibg=#2d2d2d
hi CursorColumn                 guibg=#2d2d2d
hi ColorColumn                  guibg=#2d2d2d
hi MatchParen     guifg=#f1efa5 guibg=#554236 gui=bold
hi Pmenu          guifg=#f6f3e8 guibg=#444444
hi PmenuSel       guifg=#000000 guibg=#cae682
hi Cursor         guifg=NONE    guibg=#656565 gui=none
hi CursorLineNr   guifg=#60b99a guibg=NONE    gui=bold
hi Normal         guifg=#f1efa5 guibg=#242424 gui=none
hi NonText        guifg=#000000 guibg=NONE    gui=none
hi LineNr         guifg=#857b6f guibg=#000000 gui=none
hi StatusLine     guifg=#d3ce3d guibg=#444444 gui=bold
hi StatusLineNC   guifg=#857b6f guibg=#444444 gui=none
hi VertSplit      guifg=#444444 guibg=#444444 gui=none
hi Folded         guifg=#554236 guibg=#000000 gui=none
hi FoldColumn     guifg=#554236 guibg=#000000 gui=none
hi Title          guifg=#f6f3e8 guibg=NONE    gui=bold
hi Visual         guifg=#f6f3e8 guibg=#444444 gui=none
hi SpecialKey     guifg=#000000 guibg=NONE    gui=none
hi Directory      guifg=#f77825 guibg=NONE    gui=bold
hi Question       guifg=#60b99a guibg=NONE    gui=bold
hi Search         guifg=#f1efa5 guibg=#554236 gui=bold
hi ErrorMsg       guifg=#554236 guibg=NONE    gui=underline
hi WarningMsg     guifg=#60b99a guibg=NONE    gui=underline
hi MoreMsg        guifg=#f77825 guibg=NONE    gui=bold
hi ModeMsg        guifg=#f1efa5 guibg=NONE    gui=bold

"" Syntax highlighting
hi Comment        guifg=#99968b             gui=italic
hi Todo           guifg=#554236 guibg=NONE  gui=bold
hi Constant       guifg=#d3ce3d             gui=none
hi String         guifg=#229754             gui=italic
hi Identifier     guifg=#60b99a             gui=none
hi Function       guifg=#d3ce3d             gui=bold
hi Type           guifg=#60b99a             gui=bold
hi Statement      guifg=#f77825             gui=none
hi Keyword        guifg=#f77825             gui=bold
hi PreProc        guifg=#554236             gui=none
hi Number         guifg=#d3ce3d             gui=none
hi Special        guifg=#60b99a             gui=none
hi Underlined     guifg=#f77825 guibg=NONE  gui=none
hi Error          guifg=#60b99a guibg=NONE  gui=bold

"" Diff colors
hi DiffAdd        guifg=NONE    guibg=#444444 gui=none
hi DiffChange     guifg=NONE    guibg=#444444 gui=none
hi DiffDelete     guifg=#f77825 guibg=#f77825 gui=none
hi DiffText       guifg=NONE    guibg=#000000

