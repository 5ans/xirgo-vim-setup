" Maintainer:  Jacob Foell (jfoell@gmail.com)
" Last Change: 08-Mar-2018

set background=dark

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "vivacious"

"" General colors
hi CursorLine                   guibg=#2d2d2d
hi CursorColumn                 guibg=#2d2d2d
hi ColorColumn                  guibg=#2d2d2d
hi MatchParen     guifg=#f8fcc1 guibg=#cc0c39 gui=bold
hi Pmenu          guifg=#f6f3e8 guibg=#444444
hi PmenuSel       guifg=#000000 guibg=#cae682
hi Cursor         guifg=NONE    guibg=#656565 gui=none
hi CursorLineNr   guifg=#e6781e guibg=NONE    gui=bold
hi Normal         guifg=#f8fcc1 guibg=#242424 gui=none
hi NonText        guifg=#000000 guibg=NONE    gui=none
hi LineNr         guifg=#857b6f guibg=#000000 gui=none
hi StatusLine     guifg=#c8cf02 guibg=#444444 gui=bold
hi StatusLineNC   guifg=#857b6f guibg=#444444 gui=none
hi VertSplit      guifg=#444444 guibg=#444444 gui=none
hi Folded         guifg=#cc0c39 guibg=#000000 gui=none
hi FoldColumn     guifg=#cc0c39 guibg=#000000 gui=none
hi Title          guifg=#f6f3e8 guibg=NONE    gui=bold
hi Visual         guifg=#f6f3e8 guibg=#444444 gui=none
hi SpecialKey     guifg=#000000 guibg=NONE    gui=none
hi Directory      guifg=#1693a7 guibg=NONE    gui=bold
hi Question       guifg=#e6781e guibg=NONE    gui=bold
hi Search         guifg=#f8fcc1 guibg=#cc0c39 gui=bold
hi ErrorMsg       guifg=#cc0c39 guibg=NONE    gui=underline
hi WarningMsg     guifg=#e6781e guibg=NONE    gui=underline
hi MoreMsg        guifg=#1693a7 guibg=NONE    gui=bold
hi ModeMsg        guifg=#f8fcc1 guibg=NONE    gui=bold

"" Syntax highlighting
hi Comment        guifg=#99968b             gui=italic
hi Todo           guifg=#cc0c39 guibg=NONE  gui=bold
hi Constant       guifg=#c8cf02             gui=none
hi String         guifg=#e6781e             gui=italic
hi Identifier     guifg=#e6781e             gui=none
hi Function       guifg=#c8cf02             gui=bold
hi Type           guifg=#f8fcc1             gui=bold
hi Statement      guifg=#1693a7             gui=none
hi Keyword        guifg=#1693a7             gui=bold
hi PreProc        guifg=#cc0c39             gui=none
hi Number         guifg=#c8cf02             gui=none
hi Special        guifg=#e6781e             gui=none
hi Underlined     guifg=#f8fcc1             gui=bold
hi Error          guifg=#e6781e guibg=NONE  gui=bold

"" Diff colors
hi DiffAdd        guifg=NONE    guibg=#444444 gui=none
hi DiffChange     guifg=NONE    guibg=#444444 gui=none
hi DiffDelete     guifg=#1693a7 guibg=#1693a7 gui=none
hi DiffText       guifg=NONE    guibg=#000000

