# !/bin/bash

LOCAL=true

x__print_usage() {
    cat <<EOF 1>&2
Usage:

    $0 [OPTIONS] COMMAND <command_arg>

Commands
-------------------------
  clone_cust      Clone a repo using a custom git config
                      usage: clone_cust --user "<username>" --email "<email>" \\
                             --ssh-id "<path/to/ssh/key>" \\
                             --repo "<git@<git-provider>:<org>/<repo>.git"

Options
-------------------------
  --user
  --email
  --ssh-id
  --help        Show this message

EOF
    exit 1
}

x__parse_args() {
    while [ $# -gt 0 ]; do
        local key="$1"

        case "$key" in
            --user)
                GIT_USER="$2"
                shift
                ;;
            --repo)
                GIT_REPO="$2"
                shift
                ;;
            --email)
                GIT_EMAIL="$2"
                shift
                ;;
            --ssh-id)
                GIT_SSH_ID="$2"
                shift
                ;;
            --*)
                x__print_usage
                ;;
            *)
                COMMAND="$1"
                ;;
        esac
        shift
    done

    if [ -z $COMMAND ];then 
        printf "\nmust pass a command\n\n" 1>&2
        x__print_usage
    fi
}

x__git_clone_cust() {
    if [ ! -z "${GIT_SSH_ID}" ]; then
        local MOD_ID="-c core.sshCommand=ssh -i ${GIT_SSH_ID} -F /dev/null"
    fi
    if [ ! -z "${GIT_USER}" ]; then
        local MOD_USER="-c user.name=${GIT_USER}"
    fi
    if [ ! -z "${GIT_EMAIL}" ]; then
        local MOD_EMAIL="-c user.email=${GIT_EMAIL}"
    fi
    printf "executing: git clone ${MOD_USER} ${MOD_EMAIL} ${MOD_ID} ${GIT_REPO}\n" 1>&2

    git clone "${MOD_USER}" "${MOD_EMAIL}" "${MOD_ID}" "${GIT_REPO}"
}

### Script Starts Here ###
x__parse_args "$@"


case "$COMMAND" in
    clone_cust)
        if [ -z $GIT_REPO ]; then 
            printf "\nmust pass a git repo at a minimum\n\n" 1>&2
            x__print_usage
        fi
        x__git_clone_cust
        ;;
esac



