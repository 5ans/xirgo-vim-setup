export MAKER="cmake"
export CFLAGS=""
export LDFLAGS="-l:libdl.so.2              \
                -lpthread                  \
                -lonig                     \
                -lcrypto                   \
                -levent_openssl            \
                -lssl                      \
                -levent"
