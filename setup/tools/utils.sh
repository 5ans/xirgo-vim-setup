x_is_absolute_path() {
    if [ ${1:0:1} = '/' ]; then
        echo true
    else
        echo false
    fi
}

x_is_symlink_in_path() {
    local CUR_DIR=$(pwd -L)
    cd $1
    if [ $(pwd -L) = $(pwd -P) ]; then
        echo false
    else
        echo true
    fi
    cd $CUR_DIR
}

x_realpath() {
    local CUR_DIR=$(pwd -L)

    if [ $(x_is_symlink_in_path $CUR_DIR) = true -a $(x_is_absolute_path $1) = false ]; then
        cd ${CUR_DIR}/${1}
        OUT_DIR=$(pwd -P)
        cd ${CUR_DIR}
    else
        OUT_DIR=$(realpath $1)
    fi
    echo $OUT_DIR
}
