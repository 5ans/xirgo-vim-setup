# make sure to uncomment out deb-src lines in /etc/apt/sources.list, and then apt upgrade
sudo apt build-dep vim -y
sudo apt install python3.10-dev
mkdir -p ~/git
cd ~/git
git clone https://github.com/vim/vim
cd vim
./configure --with-features=huge --enable-python3interp \
            --with-python3-config-dir=/usr/lib/python3.10/config-3.10-x86_64-linux-gnu
make -j
sudo make install
