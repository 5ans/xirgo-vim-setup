let g:airline#themes#headache#palette = {}

function! airline#themes#headache#refresh()

    let s:normal1   = [ "#60b99a", "#283439", 153, 239 ]
    let s:normal2   = [ "#182429", "#305c4d", 235, 81 ]
    let s:normal3   = [ "#283439", "#60b99a", 66, 153 ]

    let s:insert1   = [ "#d3ce3d", "#4a4b1f", 185, 238 ]
    let s:insert2   = [ "#282828", "#79772e", 235, 142 ]
    let s:insert3   = [ "#4a4b1f", "#d3ce3d", 242, 185 ]

    let s:replace1  = [ "#229754", "#082615", 203, 237 ]
    let s:replace2  = [ "#229754", "#114b2a", 235, 160 ]
    let s:replace3  = [ "#114b2a", "#229754", 237, 203 ]
    "let s:replace1  = [ "#f1efa5", "#3c3b29", 203, 237 ]
    "let s:replace2  = [ "#282828", "#787752", 235, 160 ]
    "let s:replace3  = [ "#3c3b29", "#f1efa5", 237, 203 ]

    let s:visual1   = [ "#f77825", "#3d1e09", 180, 58 ]
    let s:visual2   = [ "#f77825", "#7b3c12", 235, 215 ]
    let s:visual3   = [ "#3d1e09", "#f77825", 58, 180 ]

    let s:inactive1 = [ "#787752", "#554236", 250, 238 ]
    let s:inactive2 = [ "#787752", "#453226", 250, 242 ]
    let s:inactive3 = [ "#787752", "#352216", 250, 242 ]

    let g:airline#themes#headache#palette.normal    = airline#themes#generate_color_map(s:normal1, s:normal2, s:normal3)
    let g:airline#themes#headache#palette.insert    = airline#themes#generate_color_map(s:insert1, s:insert2, s:insert3)
    let g:airline#themes#headache#palette.replace   = airline#themes#generate_color_map(s:replace1, s:replace2, s:replace3)
    let g:airline#themes#headache#palette.visual    = airline#themes#generate_color_map(s:visual1, s:visual2, s:visual3)
    let g:airline#themes#headache#palette.inactive  = airline#themes#generate_color_map(s:inactive1, s:inactive2, s:inactive3)

    let g:airline#themes#headache#palette.normal.airline_warning   = s:visual1
    let g:airline#themes#headache#palette.insert.airline_warning   = s:visual1
    let g:airline#themes#headache#palette.replace.airline_warning  = s:visual1
    let g:airline#themes#headache#palette.visual.airline_warning   = s:visual3

    let g:airline#themes#headache#palette.tabline = {}
    let g:airline#themes#headache#palette.tabline.airline_tab      = s:normal2 
    let g:airline#themes#headache#palette.tabline.airline_tablabel = s:visual1
    let g:airline#themes#headache#palette.tabline.airline_tabhid   = s:normal1
    let g:airline#themes#headache#palette.tabline.airline_tabfill  = s:normal1
    let g:airline#themes#headache#palette.tabline.airline_tabsel   = s:normal3
    let g:airline#themes#headache#palette.tabline.airline_tabmod   = s:visual2

endfunction

call airline#themes#headache#refresh()


