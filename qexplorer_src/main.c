#include <getopt.h>

#include "quectel_include.h"
#include "quectel_efs.h"
#include "quectel_debug.h"
#include "quectel_comm.h"
#include "quectel_serial.h"

#define STRINGFY_(v) #v
#define STRINGFY(v) STRINGFY_(v)
#define VERSION_STR() STRINGFY(MAJOR) "." STRINGFY(MINOR) "." STRINGFY(REVISION)

const char *port = NULL;       //port diagport
char *local_path = "./";       //filename locally
char *remote_path = "/datatx"; //path in module where file will be located.
int debug = 0;

#define exit_if_not_exist(fpath)                                               \
    {                                                                          \
        if ((!fpath || fpath[0] == '\0' || access(fpath, F_OK)))               \
        {                                                                      \
            fprintf(stderr, "fpath(%s) is invalid or cannot access", fpath); \
            goto __error__;                                                    \
        }                                                                      \
    }

static void usage(const char *prog)
{
    dbg("Usage: %s [-r][path] [f][filename] [m][operation]", prog);
    dbg("    -r [remote path]        remote path in the modem, default \"/datatx\"");
    dbg("    -f [local path]         local filename which will be transfered or saved");
    dbg("    -m [mode]               work mode, default 'list'");
    dbg("                              delete   -> delete remote file");
    dbg("                              download -> download remote file");
    dbg("                              upload   -> upload local file to remote path");
    dbg("                              list     -> show remote files");
    dbg("                              mkdir    -> create directoy");
    dbg("                              rmdir    -> delete directoy");
    dbg("                              reset    -> reset device");
}

static void handle_remove_cmd(char *path)
{
    dbg("remove %s %s", path, (remove_file((uint8 *)path)) ? "error" : "ok");
}

static void handle_dir_cmd(char *path, int ismkdir)
{
    int ret;

    if (ismkdir)
        ret = create_dir((uint8 *)path);
    else
        ret = enumerate_dir((uint8 *)path, remove_dir_all);

    dbg("operation %s", ret ? "failed" : "success");
}

static void handle_list_cmd(char *path)
{
    if (enumerate_dir((uint8 *)path, showup) != 0)
    {
        dbg("list %s error", path);
    }
}

static void handle_reset(void)
{
    if (reset_device() != 0) {
        dbg("reset error");
    }
}

static void handle_transfer_cmd(char *remote_path, char *local_path, int isupload)
{
    int ret;
    // enumerate_dir((uint8*)remote_path, showup);
    if (isupload)
        ret = transfer_file((uint8 *)remote_path, local_path);
    else
        ret = fetch_file((uint8 *)remote_path, local_path);

    if (ret != 0)
    {
        dbg("transfer %s to %s failed, for %s", remote_path, local_path, get_transfer_error(ret));
        return;
    }
    // enumerate_dir((uint8*)remote_path, showup);
}

int main(int argc, char *argv[])
{
    int opt;
    enum
    {
        OPT_LIST,
        OPT_DELETE,
        OPT_DOWNLOAD,
        OPT_UPLOAD,
        OPT_MKDIR,
        OPT_RMDIR,
        OPT_RESET,
    } operation = OPT_LIST;

    /*build Vmajor.minor.revision*/
    dbg_time("QEFSExplorer Version: %s, build at: %s %s", VERSION_STR(), __DATE__, __TIME__);
    while ((opt = getopt(argc, argv, "r:f:m:vh")) > 0)
    {
        switch (opt)
        {
        case 'f':
            local_path = optarg;
            break;
        case 'r':
            remote_path = optarg;
            break;
        case 'm':
        {
            if (!strncasecmp(optarg, "list", 4))
                operation = OPT_LIST;
            else if (!strncasecmp(optarg, "delete", 4))
                operation = OPT_DELETE;
            else if (!strncasecmp(optarg, "download", 4))
                operation = OPT_DOWNLOAD;
            else if (!strncasecmp(optarg, "upload", 4))
                operation = OPT_UPLOAD;
            else if (!strncasecmp(optarg, "mkdir", 5))
                operation = OPT_MKDIR;
            else if (!strncasecmp(optarg, "rmdir", 5))
                operation = OPT_RMDIR;
            else if (!strncasecmp(optarg, "reset", 5))
                operation = OPT_RESET;
            else
            {
                dbg("invalid argument: %s", optarg);
                return -1;
            }
        }
        break;
        case 'v':
            debug = 1;
            break;
        case 'h':
            usage(argv[0]);
            return 0;
        default:
            break;
        }
    }

    //1. detect diagport.
    detect_diag_port((char **)&port);

    if (port)
        dbg("detect port = %s", port);
    else
        goto __error__;

    port_connect((char *)port);
    if (init_module(0) != 0)
    {
        dbg("ping module error!");
        goto __error__;
    }

    if (operation == OPT_LIST)
    {
        //list dir
        handle_list_cmd(remote_path);
    }
    else if ((operation == OPT_RESET))
    {
        dbg("doing reset");
        handle_reset();
    }
    else if ((operation == OPT_RMDIR || operation == OPT_MKDIR))
    {
        dbg("%s directory: %s", remote_path, ((operation == OPT_MKDIR) ? "MKDIR" : "RMDIR"));
        handle_dir_cmd(remote_path, operation == OPT_MKDIR);
    }
    else if (operation == OPT_DELETE)
    {
        dbg("remote file: %s", remote_path);
        handle_remove_cmd(remote_path);
    }
    else if (operation == OPT_DOWNLOAD || operation == OPT_UPLOAD)
    {
        // download/upload file from module
        if (operation == OPT_UPLOAD)
            exit_if_not_exist(local_path);

        if (operation == OPT_DOWNLOAD)
        {
            if (!local_path)
                local_path = rindex(remote_path, '/') + 1;
        }

        dbg("remote file: %s", remote_path);
        dbg("local file: %s", local_path);
        handle_transfer_cmd(remote_path, local_path, operation == OPT_UPLOAD);
    }

__error__:
    port_disconnect();
    if (port)
        free((void *)port);

    return 0;
}
