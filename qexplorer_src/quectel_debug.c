#include "quectel_debug.h"

static const char *get_time(void)
{
    static char time_buf[50];
    struct timeval tv;
    time_t time;
    suseconds_t millitm;
    struct tm *ti;

    gettimeofday(&tv, NULL);

    time = tv.tv_sec;
    millitm = (tv.tv_usec + 500) / 1000;

    if (millitm == 1000)
    {
        ++time;
        millitm = 0;
    }

    ti = localtime(&time);
    sprintf(time_buf, "[%02d-%02d_%02d:%02d:%02d:%03d]", ti->tm_mon + 1, ti->tm_mday, ti->tm_hour, ti->tm_min, ti->tm_sec, (int)millitm);
    return time_buf;
}

static pthread_mutex_t printfMutex = PTHREAD_MUTEX_INITIALIZER;
static char line[1024];
void dbg_time(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    pthread_mutex_lock(&printfMutex);
#ifdef ANDROID
    vsnprintf(line, sizeof(line), fmt, args);
    RLOGD("%s", line);
#else
#ifdef LINUX_RIL_SHLIB
    line[0] = '\0';
#else
    snprintf(line, sizeof(line), "%s ", get_time());
#endif
    vsnprintf(line + strlen(line), sizeof(line) - strlen(line), fmt, args);
    fprintf(stdout, "%s\n", line);
#endif
#if 0
    if (logfilefp) {
        fprintf(logfilefp, "%s\n", line);
    }
#endif
    pthread_mutex_unlock(&printfMutex);
}
